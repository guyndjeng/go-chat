var gulp = require("gulp"),
    notify = require("gulp-notify")
    del = require("del");

function onError(error) {
    console.log(error);
}

var stream = gulp.task('copy-assets', function() {
    return gulp.src(['bower_components/*'])
        .pipe(gulp.dest('chat/public/bower_components'))
        .pipe(notify({ message: 'File successfully copied!' }));
});

stream.on('end',function () {
    notify({ message: 'File successfully copied!' });
    del(['bower_components']).then(function(path){
        console.log('deleted ' + path);
    });
});

stream.on('error',function (error) {
    onError(error)
});
