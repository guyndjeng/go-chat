package main

import (
	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"sync"
	"os"
	"go-chat/trace"
)

type templateHandler struct {
	once     sync.Once
	filename string
	template *template.Template
}

func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.once.Do(func() {
		t.template = template.Must(template.ParseFiles(filepath.Join("templates", t.filename)))
	})
	t.template.Execute(w, nil)
}

func main() {
	mux := http.NewServeMux()
	room := newRoom()
	room.tracer = trace.New(os.Stdout)

	//web server setup
	staticFiles := http.FileServer(http.Dir("public"))
	mux.Handle("/static/", http.StripPrefix("/static/", staticFiles))
	mux.Handle("/", &templateHandler{filename: "index.html"})
	mux.Handle("/room", room)

	//starting the room
	go room.run()

	if err := http.ListenAndServe(":8088", mux); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
