package trace

import (
	"bytes"
	"testing"
)

func TestNew(t *testing.T) {
	var buffer bytes.Buffer
	tracer := New(&buffer)
	if tracer == nil {
		t.Error("Return from New should not be nil")
	} else {
		//add trace trace call
		tracer.Trace("Hello trace package")
		if buffer.String() != "Hello trace package" {
			t.Errorf("Trace should not write '%s'.", buffer.String())
		}
	}
}
